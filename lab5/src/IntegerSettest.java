public class IntegerSettest {
    public static void main(String[] args){
        IntegerSet a = new IntegerSet();
        a.insertElement(8);
        a.insertElement(32);
        a.insertElement(88);
        System.out.println(a);
        IntegerSet b = new IntegerSet();
        b.insertElement(8);
        b.insertElement(39);
        b.insertElement(90);
        System.out.println(b);
        IntegerSet c = IntegerSet.union(a, b);
        System.out.println(c);
        IntegerSet d = IntegerSet.intersection(a, b);
        System.out.println(d);
        System.out.println(c.equals(a));
        c.deleteElement(39);
        c.deleteElement(90);
        System.out.println(c.equals(a));
    }
}
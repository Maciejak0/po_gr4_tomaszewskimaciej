public class zad1test {
    public static void main(String[] args){
        zad1 saver1 = new zad1(3000);
        zad1 saver2 = new zad1(6000);

        zad1.setRocznaStopaProcentowa(0.04);

        saver1.obliczMiesieczneOdsetki();
        System.out.printf("%6.2f\n", saver1.getSaldo());

        saver2.obliczMiesieczneOdsetki();
        System.out.printf("%6.2f\n", saver2.getSaldo());

        zad1.setRocznaStopaProcentowa(0.05);

        saver1.obliczMiesieczneOdsetki();
        System.out.printf("%6.2f\n", saver1.getSaldo());

        saver2.obliczMiesieczneOdsetki();
        System.out.printf("%6.2f\n", saver2.getSaldo());
    }
}
package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println("Zad_1");
        System.out.println(31+29+31);

        System.out.println("Zad_2");
        System.out.println(1+2+3+4+5+6+7+8+9+10);

        System.out.println("Zad_3");
        System.out.println(2 * 3 * 4 * 5 * 6 * 7 * 8 * 9 * 10);

        System.out.println("Zad_4");
        System.out.println(1000*0.94);
        System.out.println(940*0.94);
        System.out.println(883.6*0.94);


        System.out.println("Zad_5");
        System.out.println("+----+");
        System.out.println("|Java|");
        System.out.println("+----+");

        System.out.println("Zad_6");
        System.out.println("  ///// ");
        System.out.println(" +\"\"\"\"\"+ ");
        System.out.println("(| 0 0 |)");
        System.out.println("  | - | ");
        System.out.println("   (_)    ");
        System.out.println(" |     | ");
        System.out.println(" +-----+ ");

        System.out.println("Zad_7");
        System.out.println("**           **                                       ***********");
        System.out.println("*  *       *  *                                            *                                                                         *");
        System.out.println("*    *   *    *   ****                                     *                          ****                                           *");
        System.out.println("*      *      *       *    ****  *   *****       *         *    ****      *** ***         *   ***  *****   *****   *       *   ***   *  *    *");
        System.out.println("*             *   *****  **         *     *      *         *   *    *    *   *   *    *****  *        *   *     *  *       *  *      * *");
        System.out.println("*             *  *    *  *       *  *******      *         *  *      *   *   *   *   *    *   ***    *    *******  *   *   *   ***   ***     *");
        System.out.println("*             *  *   **  **      *  *        *   *         *   *    *    *   *   *   *   **      *  *     *        * *   * *      *  *  *    *");
        System.out.println("*             *   *** *    ****  *   *****    ***          *    ****     *   *    *   *** *  ****  *****   *****   *       *  ****   *  ***  *");

        System.out.println("Zad_8");
        System.out.println("   +   ");
        System.out.println("  + +  ");
        System.out.println(" +   + ");
        System.out.println("+-----+");
        System.out.println("| ._. |");
        System.out.println("| | | |");
        System.out.println("+-+-+-+ ");

        System.out.println("Zad_9");
        System.out.println("          /|___/|      ");
        System.out.println("        (_  ▲_▲  )         _______      ");
        System.out.println("    _    )      (        /  WELL,  \\    ");
        System.out.println("   ((   /        |      <   HELLO   |    ");
        System.out.println("    (   )  | | | |       \\  THERE  /       ");
        System.out.println("    '___'  '_' '_'         -------             ");

        System.out.println("Zad_10");
        System.out.println("Ulubione filmy: \nSkazani na Shawshank \nDrive\nPiraci z Karaibów\n");

        System.out.println("Zad_11");
        System.out.println("Z rana wykłady, potem ćwiczenia,\nI każdy z nas ma wielkiego lenia,\nNotatki na kolosy wszyscy zbieramy,\nA i tak na poprawy często uczęszczamy,\nW końcu kolokwia udaje się zdać,\nI na egzamin pora już gnać,\nStresu pełno co niemiara,\nJak obleję będzie siara,\nLecz tu z nieba jest ratunek,\nGdyż możemy wziąć warunek.\nPuenta wiersza jest the best,\nGdyż warunek drogi jest.\nBądź cierpliwy i rozważny,\nBy zdać egzamin każdy.");

        System.out.println("zad_12");
        System.out.println(" _________________________________________________________");
        System.out.println("|* * * * * * * * *|=======================================|");
        System.out.println("| * * * * * * * * |=======================================|");
        System.out.println("|* * * * * * * * *|=======================================|");
        System.out.println("| * * * * * * * * |=======================================|");
        System.out.println("|* * * * * * * * *|=======================================|");
        System.out.println("| * * * * * * * * |=======================================|");
        System.out.println("|=========================================================|");
        System.out.println("|=========================================================|");
        System.out.println("|=========================================================|");
        System.out.println("|=========================================================|");
        System.out.println("|=========================================================|");
        System.out.println("|=========================================================|");
        System.out.println(" ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
    }
}

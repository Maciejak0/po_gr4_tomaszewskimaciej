import pl.imiajd.Tomaszewski.BetterRectangle;

public class BetterRectangleTest {
    public static void main(String[] args){
        BetterRectangle prostokat = new BetterRectangle(7, 14);
        System.out.println("Obwód: " + prostokat.getArea() + ", Pole: " + prostokat.getPerimeter());
    }
}
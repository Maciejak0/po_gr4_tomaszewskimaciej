import pl.imiajd.Tomaszewski.*;
public class OsobyTest {
    public static void main(String[] args){
        Osoba czlowiek1 = new Osoba("Tomaszewski", 2000);
        Student czlowiek2 = new Student("Rutkowski", 2001, "Informatyka");
        Nauczyciel czlowiek3 = new Nauczyciel("Wykładowca", 1972, 4100);
        System.out.println(czlowiek1.toString());
        System.out.println(czlowiek2.toString());
        System.out.println(czlowiek3.toString());
        System.out.println(czlowiek1.getNazwisko());
        System.out.println(czlowiek2.getKierunek());
        System.out.println(czlowiek3.getPensja());
    }
}
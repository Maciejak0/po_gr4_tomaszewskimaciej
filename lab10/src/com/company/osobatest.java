package com.company;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class osobatest {
    public static void main(String[] args){
        ArrayList<osoba> grupa = new ArrayList<>(5);
        grupa.add(new osoba("Tomaszewski", LocalDate.of(2000, 4, 18)));
        grupa.add(new osoba("Rutkowski", LocalDate.of(1999, 4, 14)));
        grupa.add(new osoba("Tomczak", LocalDate.of(2002, 7, 9)));
        grupa.add(new osoba("Wielgosz", LocalDate.of(2002, 7, 9)));
        System.out.println(grupa);
        Collections.sort(grupa);
        System.out.println(grupa);
    }
}

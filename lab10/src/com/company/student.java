package com.company;
import java.time.LocalDate;

public class student extends osoba implements Cloneable, Comparable<osoba> {
    private double sredniaocen;
    public student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko, dataUrodzenia);
        this.sredniaocen = sredniaOcen;
    }
    @Override
    public int compareTo(osoba o){
        if ((super.compareTo(o) == 0)&&(o instanceof student)){
            if (this.sredniaocen == ((student) o).sredniaocen){
                return 0;
            }
        }
        return 1;
    }
    @Override
    public String toString(){
        return this.getClass().getSimpleName() + " [" + super.getNazwisko() + ", " + super.getDataUrodzenia() + ", " +this.sredniaocen +"]";
    }
}

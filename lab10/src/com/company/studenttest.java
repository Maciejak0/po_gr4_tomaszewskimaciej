package com.company;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class studenttest {
    public static void main(String[] args){
        ArrayList<student> grupa = new ArrayList<>(5);
        grupa.add(new student("Tomaszewski", LocalDate.of(2000, 4, 18), 4.0));
        grupa.add(new student("Rutkowski", LocalDate.of(1999, 4, 14), 4.76));
        grupa.add(new student("Tomczak", LocalDate.of(2002, 7, 9), 5.0));
        grupa.add(new student("Wielgosz", LocalDate.of(2002, 7, 9), 4.40));
        System.out.println(grupa);
        Collections.sort(grupa);
        System.out.println(grupa);
    }
}

package com.company;

import java.util.Scanner;

import static java.lang.Math.pow;



public class zad_2 {

    public static int silnia(int liczba) {
        int x5 = 1;
        for (int i = 1;i <= liczba; i++)
        {
            x5 *= i;
        }
        return x5;
    }

    public static void main(String[] args) {

        System.out.println("Zad_2_a) ");
        Scanner scan1 = new Scanner(System.in);
        System.out.println("Ilosc liczb: ");
        int n1 = scan1.nextInt();
        int x1 = 0;

        for (int i = 0; i < n1; i++)
        {
            System.out.println("Podaj liczbe N: ");
            int a = scan1.nextInt();
            if (a < 0)
            {
                System.out.println("Podana liczba nie jest naturalna, podaj poprawną liczbe");
                break;
            }
            if (a % 2 == 1)
            {
                x1 += 1;
            }
            System.out.println("w podanych liczbach są " + x1 + " liczby nieparzyste");
        }

        System.out.println("Zad_2_b) ");
        Scanner scan2 = new Scanner(System.in);
        System.out.println("Ilosc liczb: ");
        int n2 = scan2.nextInt();
        int x2 = 0;

        for (int i = 0; i < n2; i++) {
            System.out.println("Podaj liczbe N: ");
            int a = scan2.nextInt();

            if (a < 0) {
                System.out.println("Podana liczba nie jest naturalna, podaj poprawną liczbe");
                break;
            }
            if (a % 3 == 0 && a % 5 != 0) {
                x2 += 1;
            }
            System.out.println("w podanych liczbach jest " + x2 + " podzielna przez 3 i niepodzielna przez 5");
        }

        System.out.println("Zad_2_c) ");
        Scanner scan3 = new Scanner(System.in);
        System.out.println("Ilosc liczb: ");
        int n3 = scan3.nextInt();
        int x3 = 0;

        for (int i = 0; i < n3; i++) {
            System.out.println("Podaj liczbe N: ");
            int a = scan3.nextInt();

            if (a < 0) {
                System.out.println("Podana liczba nie jest naturalna, podaj poprawną liczbe");
                break;
            }
            if (pow(a, a) % 2 == 0){
                x3 += 1;
            }
            System.out.println("w podanych liczbach jest " + x3 + " ktore są kwadratami liczby parzystej ");
        }


        System.out.println("Zad_2_d) ");
        Scanner scan4 = new Scanner(System.in);
        System.out.println("Ilosc liczb: ");
        int n4 = scan4.nextInt();
        int x4 = 0;
        int[]y= new int[n4];

        for (int i = 0; i < n4; i++) {
            System.out.println("Podaj liczbe N: ");
            int a = scan4.nextInt();
            y[i] = a;

            if (a < 0) {
                System.out.println("Podana liczba nie jest naturalna, podaj poprawną liczbe");
                break;
            }
        }
        for (int i = 0; i < n4-1; i++)
        {
            if (i>0 && ((y[i-1] + y[i+1])/2) > y[i]){
                x4 += 1;
            }
            System.out.println("w podanych liczbach jest " + x4 + " spelniających warunek polecenia");
        }

        System.out.println("Zad_2_e) ");
        Scanner scan5 = new Scanner(System.in);
        System.out.println("Ilosc liczb: ");
        int n5 = scan5.nextInt();
        int[]x = new int[n5];
        int x5 = 0;

        for (int i=0; i < n5; i++) {
            System.out.println("Podaj liczbe N: ");
            int a = scan5.nextInt();
            x[i] = a;

            if (a < 0) {
                System.out.println("Podana liczba nie jest naturalna, podaj poprawną liczbe");
                break;
            }
        }
        for (int i = 0; i < n5; i++)
        {
            if ((pow(2, i) < x[i]) && (silnia(i) > x[i]))
            {
                x5 += 1;
            }
            System.out.println("w podanych liczbach jest " + x5 + " spelniających warunek polecenia");
        }

        System.out.println("Zad_2_f) ");
        Scanner scan6 = new Scanner(System.in);
        System.out.println("Ilosc liczb: ");
        int n6 = scan6.nextInt();
        int z[] = new int[n6];
        int x6 = 0;

        for (int i = 0; i < n6; i++) {
            System.out.println("Podaj liczbe N: ");
            int a = scan6.nextInt();
            z[i] = a;
        }
        for (int i = 0; i < n6; i++)
        {
            if ((i+1) % 2 == 1 && z[i] % 2 == 0)
            {
                x6 += 1;
            }
        }
        System.out.println("w podanych liczbach jest " + x6 + " spelniających warunek polecenia");


        System.out.println("Zad_2_g) ");
        Scanner scan7 = new Scanner(System.in);
        System.out.println("Ilosc liczb: ");
        int n7 = scan7.nextInt();
        int x7 = 0;

        for (int i = 0; i < n7; i++)
        {
            System.out.println("Podaj liczbe N: ");
            int a7 = scan7.nextInt();

            if (a7 < 0)
            {
                System.out.println("Podana liczba nie jest naturalna, podaj poprawną liczbe");
                break;
            }
            if (a7 % 2 == 1){
                x7 += 1;
            }
            System.out.println("w podanych liczbach jest " + x7 + " spelniających warunek polecenia");
        }


    }


}

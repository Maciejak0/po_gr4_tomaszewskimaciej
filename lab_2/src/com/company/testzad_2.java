package com.company;

import java.util.Random;
import java.util.Scanner;

public class testzad_2 {
    static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Podaj liczbe z zakresu 1<=n<=100: ");
        int n = in.nextInt();
        if(n>100 || n<1) {
            System.out.println("Podana liczba nie pasuje do zakresu 1<=n<=100.");
            n = in.nextInt();
        }
        int[] tab = new int[n];
        zad_2.generuj(tab, n, -999, 999);

        int a = zad_2.ileNieparzystych(tab);
        int b = zad_2.ilePatrzystych(tab);
        int c = zad_2.ileDodatnich(tab);
        int d = zad_2.ileUjemnych(tab);
        int e = zad_2.ileZerowych(tab);
        int f = zad_2.ileMaksymalnych(tab);
        int g = zad_2.sumaDodatnich(tab);
        int h = zad_2.sumaUjemnych(tab);
        int i = zad_2.dlugoscMaksymaknegoCiaguDodatnich(tab);
        System.out.printf("%d, %d, %d, %d, %d, %d, %d, %d, %d", a, b, c, d, e, f, g, h, i);
    }
}
